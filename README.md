# Just in time 2: It's about time

Just in time provided a data visualisation project for DUNE exactly when it was needed. Although it worked, it was a small scaled project with no expantion capabilities, and an expandable version of the project was by now long overdue. "It's about time" aims to provide an implementation of the Just in time functionality with code that's easily expandable and much more robust file structure.
