import dash
from dash import dcc
from dash import html
from dash.dependencies import Input, Output
from ..plots import plots
from . import pages

class page:
	def __init__(self, name, url):
		self.name = name
		self.url = url
		self.plots =[]
	
	def add_plot(self, plot):
		if isinstance(plot, list):
			self.plots.extend(plot)
		elif isinstance(plot, str):
			self.plots.append(plot)

	def init_dashboard(self, server):
		dash_app = dash.Dash(server = server, routes_pathname_prefix =f"/{self.url}/")

		page_buttons = pages.give_pages()
		page_links = []
		for button in page_buttons:
			page_links.append(dcc.Link(f"|   {button.name}   |", href = f"/{button.url}", refresh=True))
		plot_list = plots.give_plots(dash_app)
		options = []
		contents = []
		for plot in plot_list:
			if plot.name in self.plots:
				plot.initiate_plot()
				options.append(plot.options)
				contents.append(plot.contents)
		page_div = html.Div([
			dcc.Link('Back', href='/', refresh=True),
			html.Div(page_links, style={'display': 'flex', 'flex-direction': 'row'}),
			html.H3(self.name),
			html.Div(options),
			html.Div(contents)
		], id ="wrapper")
		dash_app.layout = page_div

		return(dash_app.server)