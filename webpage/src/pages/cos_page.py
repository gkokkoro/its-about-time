from . import page_class

def return_page():
	page = page_class.page("Cos_Plot", "cos")
	page.add_plot("cos_plot")
	return(page)