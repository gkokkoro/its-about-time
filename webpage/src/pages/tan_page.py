from . import page_class

def return_page():
	page = page_class.page("Tan_Plot", "tan")
	page.add_plot("tan_plot")
	return(page)