import os
import importlib

new_pages = True
global_pages = []

def init_pages():
	ignore = ["pages.py", "page_class.py", "__pycache__"]
	pages = []

	file_list = os.listdir("./src/pages/")
	for file in file_list:
		if file not in ignore:
			module = importlib.import_module(f".pages.{file[:-3]}", "src")
			pages.append(module.return_page())

	return(pages)

def give_pages():
	global new_pages, global_pages
	if new_pages:
		new_pages = False
		global_pages = init_pages()
	return(global_pages)