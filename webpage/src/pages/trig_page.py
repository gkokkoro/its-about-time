from . import page_class

def return_page():
	page = page_class.page("Trig_functions", "trig")
	page.add_plot("sin_plot")
	page.add_plot("cos_plot")
	page.add_plot("tan_plot")
	return(page)