"""Initialize Flask app."""
from flask import Flask
from .pages import pages as pg


def init_app():
	"""Construct core Flask application with embedded Dash app."""
	app = Flask(__name__)
	pages = pg.give_pages()

	with app.app_context():
		# Import parts of our core Flask app
		from . import routes

		 #Import Dash application
		for page in pages:
			app = page.init_dashboard(app)


		return(app)