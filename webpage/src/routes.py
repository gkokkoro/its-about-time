from flask import render_template, redirect, request
from flask import current_app as app
from .pages import pages

@app.route('/', methods=['GET', 'POST'])
def home():
	page_buttons = pages.give_pages()
	if request.method == 'POST':
		for button in page_buttons:
			if request.form.get(button.url) == button.name:
				return(redirect(f'/{button.url}'))
	elif request.method == 'GET':
		return render_template('buttons.html', form="form", buttons = page_buttons)
	
	return render_template("buttons.html", buttons = page_buttons)