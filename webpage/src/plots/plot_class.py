import dash
from dash import html

class plot:
	def __init__(self, app, name):
		self.options = html.Div()
		self.contents = html.Div()
		self.app = app
		self.name = name

	def add_options(self, options):
		self.options = options
	
	def add_content(self, content):
		self.contents = content
	
	def add_initiation_function(self, initiation_function):
		self.initiation_function = initiation_function
	
	def initiate_plot(self):
		self.initiation_function(self)