import os
import importlib

def give_plots(dash_app):
	ignore = ["plots.py", "plot_class.py", "__pycache__"]
	plots = []

	file_list = os.listdir("./src/plots/")
	for file in file_list:
		if file not in ignore:
			module = importlib.import_module(f".plots.{file[:-3]}", "src")
			plots.append(module.return_plot(dash_app))

	return(plots)
