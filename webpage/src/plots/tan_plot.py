from . import plot_class
from dash import html, dcc
import dash_daq as daq
import plotly.graph_objects as go
from dash.dependencies import Input, Output, State
import numpy as np

def return_plot(dash_app):
	plot = plot_class.plot(dash_app, "tan_plot")

	def initiate_plot(plot):
		options = html.Div([
			html.Div(" "),
			daq.Slider(
				id="tan_period_slider", 
				min = 1, max =4, value = 2, 
				handleLabel={"showCurrentValue": True,"label": "Period"}, 
				step = 0.1)
		])

		content = html.Div(id = "tan_contents")

		@plot.app.callback(
			Output("tan_contents", "children"),
			Input("tan_period_slider", "value")
		)
		def plot_tan_graph(period):
			x_axis = np.linspace(0, 10, 10000)
			y_axis = np.tan(2*x_axis/period)
			fig = go.Figure()
			fig.add_trace(go.Scatter(x = x_axis, y = y_axis, mode='markers'))
			return(html.Div([html.H4('Tan plot'), dcc.Graph(id = "tan_plot", figure = fig)]))

		plot.add_options(options)
		plot.add_content(content)
	
	plot.add_initiation_function(initiate_plot)
	return(plot)